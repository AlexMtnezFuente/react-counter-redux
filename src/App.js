import React from 'react';
import {connect} from 'react-redux';
import * as actions from './actions';

const App = ({value, onIncrement, onDecrement, onReset}) => (
    <div>
        {value + ' '}
        <button onClick={onIncrement}>+</button>
        {' '}
        <button onClick={onDecrement}>-</button>
        {' '}
        <button onClick={onReset}>Reset</button>
    </div>
);

/* Función que mapea a propiedades la información 
del nuevo estado producido por el reducer. */
const mapStateToProps = state => ({
    value: state.value
});

/* Función que envía diferentes acciones a la store. */
const mapDispatchToProps = dispatch => ({
    onIncrement: () => dispatch(actions.increment()),
    onDecrement: () => dispatch(actions.decrement()),
    onReset: () => dispatch(actions.reset())
});

/* Suscribimos el componente a la store. */
export default connect(mapStateToProps, mapDispatchToProps)(App);
/* La anterior línea es equivalente a las siguientes líneas de código:
const connector = connect(mapStateToProps, mapDispatchToProps);
//connector(App) devuelve el componente App suscrito a la store.
const connectedAppComponent = connector(App); 
export default connectedAppComponent; */