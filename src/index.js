import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './App';
import reducer from './reducer';

/* Creamos el store. */
const store = createStore(reducer);

/* El componente Provider permite pasar el store a 
todos los componentes en el árbol de componentes y 
permite que los componentes se puedan suscribir a
la store. */
ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>, 
    document.getElementById('root')
);
